#include <stdio.h>

static int n1=0,n2=1; 

int fibsq(int term){    
    if(term>1){
		fibsq(term-1);        
        term=n1+n2;       
        printf("%d ",term);
        n1=n2;
		n2=term;    
    }
}
	
int main(){
	int count;
	printf("ENTER A TERM: ");
	scanf("%d",&count);
	printf("Fibonacci Sequence: ");
	printf("%d " "%d " ,n1,n2);
	fibsq(count);
}
